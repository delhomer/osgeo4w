Oslandia's OSGeo4W distribution
===============================

This repository hosts sources for the generation of OSGeo4W packages.

They are designed to be run on our Windows build environment through gitlab-ci.

Directory structure
--------------------

To add a package script, add a new subdirectory and put at least the following files in it:
- package.cmd: the Windows "shell" script that will be triggered to compile the package. Have a look at [some existing package script](packages/protobuf/package.cmd).
- setup.hint: this is the Cygwin/OSGeo4W definition of the package that will be read by some script to generate a setup.ini file used by osgeo4w installers

The [include directory](packages/__inc__) contains common batch files that should ease the writing of package.cmd scripts

Package building
-----------------

If you're willing to contribute to existing packages or just want to build your own package, you'd have to fork this repository and push your modifications (in master or in a specific branch). The modified pakages will be automatically built and available upon completion on http://osgeo4w-oslandia.com/extra.test.

You can then propose a Merge Request to integrate your modifications in master. Once merged, packages will be updated in main repository http://osgeo4w-oslandia.com/extra.

Be aware that you need to define a GitLab runner to build your package (see [Install GitLab runner](#install-gitlab-runner)).

You can also use Oslandia runner: Go to Settings > CI/CD > *Expand* Runners > Specific Runners > Runners activated for this project > *Enable* osgeo4w win10-vs2017.

It's also possible to trigger a build by launching a [scripts/README.md](script on the command line).

OSGeo4W Setup
-------------

When using an OSGeo4W setup, point it to http://osgeo4w-oslandia.com/mirror if you want to only use a mirror of the official distribution. Point it to
http://osgeo4w-oslandia.com/extra if you want to benefit from the extra packages found here.

You can also call from the command line
```
osgeo4w-setup.exe -O -s http://osgeo4w-oslandia.com/extra
```

Custom Setup
------------

A better option is to wrap the previous command line in a standalone .exe file. Have a look at the [make_setup_exe](make_setup_exe/) folder.


Install local OSGEO4W Virtual mahine
------------

If you want to debug a failed job you can create and run locally your own OSGEO4W virtual machine, following [these instructions](vm_windows/README.md).


Install GitLab runner
------------

Instructions can be found [here](https://docs.gitlab.com/runner/install/linux-repository.html)

```shell
$ curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh | sudo bash
$ sudo apt-get install gitlab-runner
```

During the installation process the gitlab-runner configuration file has been created `/etc/gitlab-runner/config.toml`.

You can now create a runner with the following command

```shell
sudo gitlab-runner register
```

Fill in the appropriate information. The token can be found on your project CI/CD settings beneath *Use the following registration token during setup*. 

gitlab-runner configuration file `/etc/gitlab-runner/config.toml` has then been modified

You now have to deploy the appropriate virtual machine on your gitlab-runner instance (see [how to create one](vm_windows/README.md)).

If needed you can export/import virtual machine this way

```shell
$ vboxmanage export vm_windows_default_1586966951496_49036 -o win10-vs2017.ova
$ scp win10-vs2017.ova root@ci.oslandia.net:/home/storage/ci
$ ssh root@ci.oslandia.net
$ cd /home/storage/ci
$ vboxmanage import win10-vs2017.ova
$ vboxmanage modifyvm vm_windows_default_1586966951496_49036 --name win10-vs2017
```

You can change the number of concurrent runners in `config.toml` file
```
concurrent = 2
```




