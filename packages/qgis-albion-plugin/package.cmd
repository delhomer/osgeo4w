::--------- Package settings --------
:: package name
set P=qgis-albion-plugin
:: version
set V=2.3.0
:: package version
set B=1
:: plugin name
set N=albion

set HERE=%CD%

::--------- Prepare the environment

call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%
set PLUGINDIR=install\apps\qgis-ltr\python\plugins

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

echo ################################
python --version
echo ################################

mkdir -p %PLUGINDIR% || goto :error
wget https://gitlab.com/Oslandia/albion/uploads/e4adcbcb58b3d260d27f2e45eea4a604/Albion.zip -O %N%-%V%.zip || goto :error
unzip -d %PLUGINDIR% %N%-%V%.zip || goto :error
c:\cygwin64\bin\tar.exe --transform 's,install/,,'  -cvjf %PKG_BIN% install || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF

:error
echo Build failed
exit /b 1
