::--------- Package settings --------
:: package name
set P=qgis-thyrsis-documentation
:: version
set V=1.1.0
:: package version
set B=7

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

:: python -m pip install sip || goto :error
wget -L -O thyrsis_documentation.zip --header "PRIVATE-TOKEN:x6322vVf4oSW8HWJ2q3V" "https://gitlab.com/api/v4/projects/Oslandia%%2Fqgis%%2Fthyrsis/jobs/artifacts/master/download?job=pages"
mkdir -p apps\thyrsis_documentation\build
mkdir -p etc\postinstall
unzip thyrsis_documentation.zip
xcopy /E public\* apps\thyrsis_documentation\build\
copy postinstall.bat etc\postinstall\thyrsis_documentation_postinstall.bat

:: echo --transform 's,install/,apps/qgis-ltr/python/plugins/,' -cvjf %PKG_BIN% install
7z a -ttar %P%.tar "apps/thyrsis_documentation" "etc/postinstall" || goto :error
7z a -tbzip2 %PKG_BIN% %P%.tar || goto :error

::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
