::--------- Package settings --------
:: package name
set P=python3-scikit-bio
:: version
set V=0.5.6
:: package version
set B=1
:: name
set N=scikit-bio

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
call %OSGEO4W_ROOT%\bin\py3_env.bat
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set SITEPACKAGES=apps/Python37/Lib/site-packages

python3 -m pip install %N%==%V% || goto :error

:: archive (add dependencies too)
c:\cygwin64\bin\tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% ^
	%SITEPACKAGES%/scikit_bio-%V%-py3.7.egg-info ^
	%SITEPACKAGES%/skbio ^
	%SITEPACKAGES%/benchmarks ^
	%SITEPACKAGES%/Cython-0.29.21.dist-info ^
	%SITEPACKAGES%/Cython ^
	%SITEPACKAGES%/cython.py ^
	%SITEPACKAGES%/hdmedians ^
	%SITEPACKAGES%/hdmedians-0.14.1.dist-info ^
	%SITEPACKAGES%/pyximport ^
	%SITEPACKAGES%/IPython ^
	%SITEPACKAGES%/ipython-7.18.1.dist-info ^
	%SITEPACKAGES%/natsort ^
	%SITEPACKAGES%/natsort-7.0.1.dist-info ^
	%SITEPACKAGES%/cachecontrol ^
	%SITEPACKAGES%/CacheControl-0.12.6.dist-info ^
	%SITEPACKAGES%/lockfile ^
	%SITEPACKAGES%/lockfile-0.12.2.dist-info ^
	%SITEPACKAGES%/msgpack ^
	%SITEPACKAGES%/msgpack-1.0.0.dist-info || goto: error

::--------- Installation
scp %PKG_BIN% %R%
cd %HERE%
scp setup.hint %R%
