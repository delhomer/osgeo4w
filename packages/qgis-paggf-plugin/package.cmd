echo "on entre dans le package" 
::--------- Package settings --------
:: package name
set P=qgis-paggf-plugin
:: version
set V=1.4.0
:: package version
set B=1

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%
set PYTHONPATH=%PYTHONPATH%;%HERE%
set PLUGINDIR=%OSGEO4W_ROOT%\apps\qgis-ltr\python\plugins

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat || goto :error

rmdir /S /Q package_paggf
git clone https://gitlab+deploy-token-24:kz1p1xcvB4zohaGBwDsX@git.oslandia.net/speillet/package_paggf.git || goto :error
rmdir /S /Q package_paggf\.git
rmdir /S /Q apps
rmdir /S /Q etc
rmdir /S /Q share

:: copy database dump
mkdir share\extension
move /y package_paggf\paggf.control share\extension
move /y package_paggf\paggf--%V%.sql share\extension
:: copy QGIS plugin
mkdir apps\qgis-ltr\python\plugins
move /y package_paggf\qgis\plugin\PAGGF apps\qgis-ltr\python\plugins\

:: copy QGIS projects and conf
mkdir apps\paggf
move /y package_paggf\qgis\paggf_encodage_bm.qgs apps\paggf
move /y package_paggf\qgis\paggf_encodage_cid.qgs apps\paggf
move /y package_paggf\qgis\paggf_encodage_ef.qgs apps\paggf
move /y package_paggf\qgis\paggf_admin.qgs apps\paggf
move /y package_paggf\qgis\custom_ui.conf apps\paggf
copy /y paggf_launch.bat apps\paggf
move /y package_paggf\qgis\QGIS3.ini apps\paggf
copy /y pg_service.conf apps\paggf
copy /y icon.ico apps\paggf

:: postinstall
mkdir etc\postinstall
move /Y paggf_postinstall.bat etc\postinstall

7z a -ttar %P%.tar "apps/qgis-ltr" "apps/paggf" "etc/postinstall" "share/extension" || goto :error
7z a -tbzip2 %PKG_BIN% %P%.tar || goto :error



::--------- Installation
scp %PKG_BIN% %R% || goto :error
scp setup.hint %R% || goto :error
goto :EOF


:error
echo Build failed
exit /b 1
