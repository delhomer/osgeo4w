::--------- Package settings --------
:: package name
set P=sfcgal
:: version
set V=1.3.8
:: package version
set B=1

set HERE=%CD%

set BUILD_DEPS=cgal

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1 || goto :error

git clone https://gitlab.com/oslandia/SFCGAL.git --depth 1 --branch v%V%  || goto :err

rd /S /Q install
mkdir install
cd SFCGAL
rd /S /Q build
mkdir build
cd build

cmake -DCMAKE_BUILD_TYPE=RelWithDebInfo ^
      -DCMAKE_INSTALL_PREFIX=%HERE%\install ^
      -DGMP_INCLUDE_DIR=C:\OSGeo4W64\include ^
      -DGMP_LIBRARIES=C:\OSGeo4W64\lib\libgmp.dll.a ^
      -DMPFR_LIBRARIES=C:\OSGeo4W64\lib\libmpfr.dll.a ^
      -G "NMake Makefiles" .. || goto :error
	  
cmake --build . --target install VERBOSE=1 || goto :error

cd %HERE%

c:\cygwin64\bin\tar.exe -C install -cjvf %PKG_BIN% lib bin include || goto :error

::--------- Installation
call %HERE%\..\__inc__\install_archives.bat || goto :error

goto :EOF

:error
echo Build failed
exit /b 1
