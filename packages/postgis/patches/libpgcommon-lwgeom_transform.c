--- "libpgcommon\\lwgeom_transform.c.orig"
+++ "libpgcommon\\lwgeom_transform.c"
@@ -33,6 +33,8 @@
 #include <stdio.h>
 
 
+#define maxprojlen  512
+#define spibufferlen 512
 /**
 * Global variable to hold cached information about what
 * schema functions are installed in. Currently used by
@@ -355,8 +357,6 @@ SPI_pstrdup(const char* str)
 static PjStrs
 GetProjStringsSPI(int32_t srid)
 {
-	const int maxprojlen = 512;
-	const int spibufferlen = 512;
 	int spi_result;
 	char proj_spi_buffer[spibufferlen];
 	PjStrs strs;
@@ -444,7 +444,6 @@ GetProjStringsSPI(int32_t srid)
 static PjStrs
 GetProjStrings(int32_t srid)
 {
-	const int maxprojlen = 512;
 	PjStrs strs;
 	memset(&strs, 0, sizeof(strs));
 