::--------- Package settings --------
:: package name
set P=python3-wheel
:: version
set V=0.33.0
:: package version
set B=1

set BUILD_DEPS=python3-core

set HERE=%CD%

::--------- Prepare the environment
call ..\__inc__\prepare_env.bat %1
set OSGEO4W_ROOT=c:\osgeo4w64
set PATH=%OSGEO4W_ROOT%\bin;%PATH%

:: python3 package
call %OSGEO4W_ROOT%\bin\py3_env.bat

pip install wheel==%V%

tar -C %OSGEO4W_ROOT% -cvjf %PKG_BIN% ^
  apps/Python37/Lib/site-packages/wheel ^
  apps/Python37/Lib/site-packages/wheel-%V%.dist-info || goto :error

::--------- Installation
scp %PKG_BIN% %R%
cd %HERE%
scp setup.hint %R%
