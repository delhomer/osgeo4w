call %OSGEO4W_ROOT%\bin\o4w_env.bat
call %OSGEO4W_ROOT%\bin\py3_env.bat

curl.exe https://cloud-images.ubuntu.com/releases/focal/release/ubuntu-20.04-server-cloudimg-amd64-wsl.rootfs.tar.gz --output C:\Users\%USERNAME%\AppData\Local\Temp\Ubuntu2004.tar.gz

for /f "delims=" %%i in ('where wsl') do set wsl_cmd=%%i
if not x%wsl_cmd:wsl.exe=%==x%wsl_cmd% (wsl --import Ubuntu-20.04 C:\Ubuntu-20.04 C:\Users\%USERNAME%\AppData\Local\Temp\Ubuntu2004.tar.gz  & bash ~ --login -c "bash /mnt/c/OSGeo4W64/apps/qgis-ltr/python/plugins/thyrsis/wsl_install.sh" & exit /b 0) else (echo WSL is not installed && exit /b 1)